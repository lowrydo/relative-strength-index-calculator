
// Relative Strength Index Calculator
// Author: D�NAL LOWRY 26/02/2018

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class RSICalculator {

	public static void main(String[] args) throws FileNotFoundException {	
		
		// LOAD EXCEL FILE
		Scanner scanner = new Scanner(new File("/Users/lowry/Desktop/stocks.csv"));
		System.out.print("\nRELATIVE STRENGTH INDEX VALUES \n\n");
	     
	    while(scanner.hasNext()) {
	    	String line = scanner.nextLine();
	        String[] array = line.split(",");
	        double[] numArray = new double[array.length-1];
	        String stock_name = array[0];
	        
	        // PARSE STRING ARRAY TO DOUBLE ARRAY
	    	for (int i = 1; i < array.length; i++) {
	    		numArray[i-1] = Double.parseDouble(array[i]);
	    	}
	        
	        // CALCULATE RS 
	    	double av_gain_up_periods = 0;
	    	double av_loss_down_periods = 0;
	    	int gain_count = 0;
	    	int loss_count = 0;
	    	double previous_observation = numArray[0];
	    	
	    	for (int i = 1; i < numArray.length; i++) {
	    		if (previous_observation <= numArray[i]) { // if gain
	    			double gain = numArray[i] - previous_observation;
	    			gain_count++;
	    			av_gain_up_periods += gain;
	    		}
	    		else { // if loss
	    			double loss = previous_observation - numArray[i];
	    			loss_count++;
	    			av_loss_down_periods += loss;
	    		}
	    		previous_observation = numArray[i];
	    	}
	    	av_gain_up_periods = av_gain_up_periods/gain_count;
	    	av_loss_down_periods = av_loss_down_periods/loss_count;
	    	
	    	// CALCULATE RSI
	    	double relative_strength = av_gain_up_periods/av_loss_down_periods;
	    	double relative_strength_index = 100-(100/(1+relative_strength));
	    	
	    	// PRINT RESULT
	    	System.out.print(stock_name + ": " + relative_strength_index + "\n");
	    }
	    scanner.close();  
	}
}
